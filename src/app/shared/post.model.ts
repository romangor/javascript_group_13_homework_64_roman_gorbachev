export class Post {
  constructor(public title: string,
              public description: string,
              public date: Date,
              public id: string) {
  }
}
