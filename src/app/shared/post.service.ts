import { Post } from './post.model';
import { EventEmitter } from '@angular/core';


export class PostService {
  postsChange = new EventEmitter<Post[]>();
  posts!: Post[];

  getPost(index: number) {
    return this.posts[index];
  }

}
