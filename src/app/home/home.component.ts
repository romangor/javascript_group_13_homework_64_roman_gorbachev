import { Component, OnInit } from '@angular/core';
import { Post } from '../shared/post.model';
import { HttpClient } from '@angular/common/http';
import { PostService } from '../shared/post.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  posts!: Post[];
  loader = false;

  constructor(private http: HttpClient,
              private postService: PostService,) {
  }

  ngOnInit() {
    this.loader = true;
    this.http.get<{ [id: string]: Post }>('https://projectsattractor-default-rtdb.firebaseio.com/blog/posts.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const postData = result[id];
          return new Post(postData.title, postData.description, postData.date, id);
        });
      }))
      .subscribe(posts => {
        this.loader = false;
        this.postService.posts = posts;
        this.postService.postsChange.subscribe((posts: Post[]) =>{
          this.posts = posts;
        });
        this.posts = this.postService.posts;
      });
  }
}
