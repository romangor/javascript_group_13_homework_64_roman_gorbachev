import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {
  name!: string;
  email!: string;
  phone!: string;

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.http.get('https://projectsattractor-default-rtdb.firebaseio.com/blog/contacts.json')
      .pipe(map(result => {
        return Object(result);
      }))
      .subscribe(data => {
        this.name = data.name;
        this.email = data.email;
        this.phone = data.phone;
      })
  }

}
