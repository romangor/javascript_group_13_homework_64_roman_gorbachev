import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostsContainerComponent } from './posts-container/posts-container.component';
import { AddPostComponent } from './add-post/add-post.component';
import { PostDetailsComponent } from './posts-container/post-details/post-details.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './NotFound';
import { AboutComponent } from './about/about.component';
import { ContactsComponent } from './contacts/contacts.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {
    path: 'posts', component: PostsContainerComponent, children: [
      {
        path: ':id', component: PostDetailsComponent, children: [
          {path: ':edit/:id', component: AddPostComponent}
        ]
      },
    ]
  },
  {path: 'post/add', component: AddPostComponent},
  {path: 'about', component: AboutComponent},
  {path: 'contacts', component: ContactsComponent},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
