import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { PostsContainerComponent } from './posts-container/posts-container.component';
import { PostDetailsComponent } from './posts-container/post-details/post-details.component';
import { FormsModule } from '@angular/forms';
import { AddPostComponent } from './add-post/add-post.component';
import { HttpClientModule } from '@angular/common/http';
import { PostService } from './shared/post.service';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './NotFound';
import { AboutComponent } from './about/about.component';
import { ContactsComponent } from './contacts/contacts.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    PostsContainerComponent,
    PostDetailsComponent,
    AddPostComponent,
    HomeComponent,
    NotFoundComponent,
    AboutComponent,
    ContactsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [PostService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
