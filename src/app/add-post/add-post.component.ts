import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PostService } from '../shared/post.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Post } from '../shared/post.model';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent implements OnInit {
  title!: string;
  description!: string;
  id!: string;

  constructor(private http: HttpClient,
              private postService: PostService,
              private router: Router,
              private routeId: ActivatedRoute,) {
  }

  ngOnInit(): void {
    this.routeId.params.subscribe((params: Params) => {
      const idPost = params['id'];
      this.http.get<{ [id: string]: Post }>(`https://projectsattractor-default-rtdb.firebaseio.com/blog/posts/${idPost}.json`)
        .pipe(map(result => {
          if (result === null) {
            return [];
          }
          return Object(result);
        }))
        .subscribe(post => {
          if (!idPost) {
            this.title = post.title;
            this.description = post.description;
          }
        });
      this.id = idPost;
    });
  }

  sendPost() {
    const title = this.title;
    const description = this.description;
    const date = new Date();
    const body = {title, description, date};
    if (this.id) {
      this.http.put(`https://projectsattractor-default-rtdb.firebaseio.com/blog/posts/${this.id}.json`, body)
        .subscribe();
    } else {
      this.http.post('https://projectsattractor-default-rtdb.firebaseio.com/blog/posts.json', body)
        .subscribe((params: Params) => {
          const id = params['id'];
          this.postService.posts.push(new Post(title, description, date, id));
        });
    }
  }
}
