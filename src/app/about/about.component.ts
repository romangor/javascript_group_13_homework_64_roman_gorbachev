import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  title!: string;
  description!: string;

  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
    this.http.get<{ title: string, description: string }>('https://projectsattractor-default-rtdb.firebaseio.com/blog/about.json')
      .pipe(map(result => {
        return Object(result);
      }))
      .subscribe(data => {
        this.title = data.title;
        this.description = data.description;
      })
  }
}
