import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/shared/post.model';
import { ActivatedRoute, Params } from '@angular/router';
import { PostService } from 'src/app/shared/post.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {
  post!: Post;
  index!: number;

  constructor(private route: ActivatedRoute, private postService: PostService,
              private http: HttpClient) {
  }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const postId = parseInt(params['id']);
      this.index = postId
      this.post = this.postService.getPost(postId);
    });
  }

  onDelete() {
    const array = this.postService.posts.splice(1, this.index);
    this.postService.postsChange.emit(array);
    this.http.delete(`https://projectsattractor-default-rtdb.firebaseio.com/blog/posts/${this.post.id}.json`).subscribe();
  }
}


